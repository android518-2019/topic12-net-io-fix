#!/bin/bash
#script to test api 
#http://www.edamam.com
#2014
#ID=e6501390
#KEY=71b785fe9091261dbe192db334d77dbd
#2015
#ID=b3f7ba07
#KEY=77397e154a68c54cb1380e268edffa84	
#2019
# food db
ID=46308b95
KEY=cced96337a51df88ee66f2eb2b9c13c6
# nutrient db, 2nd key needed
KEYN=d38db7f66e1c6a7b6001dbfb4ea94e28
IDN=501a43e4
#2019 changes
# requested only food api, the others need separate id & key (?)
#need to use a get to get the foodid to use in the post request ??? 

# test food database api
fn="food-search-get.txt"
echo "parser food database search GET" 
echo "output $fn"
# https://api.edamam.com/api/food-database/parser
curl -X GET -H "Content-Type: application/json" "https://api.edamam.com/api/food-database/parser?ingr=banana&app_id=${ID}&app_key=${KEY}"  >$fn

# show the foodID, needed in the json, done manually for now
./getfoodid.sh

#test nutrition analysis api
source testapinutr.sh

#test diet recommendations api
#https://developer.edamam.com/diet_api_documentation
#curl "https://api.edamam.com/diet?q=chicken&app_id=$ID&app_key=$KEY&from=0&to=3&calories=gte%20591,%20lte%20722&health=alcohol-free" >diet.txt


