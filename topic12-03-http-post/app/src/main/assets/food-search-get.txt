{
  "text" : "banana",
  "parsed" : [ {
    "food" : {
      "foodId" : "food_bjsfxtcaidvmhaa3afrbna43q3hu",
      "label" : "Bananas",
      "nutrients" : {
        "ENERC_KCAL" : 89.0,
        "PROCNT" : 1.09,
        "FAT" : 0.33,
        "CHOCDF" : 22.84,
        "FIBTG" : 2.6
      },
      "category" : "Generic foods",
      "categoryLabel" : "food"
    }
  } ],
  "hints" : [ {
    "food" : {
      "foodId" : "food_bjsfxtcaidvmhaa3afrbna43q3hu",
      "label" : "Bananas",
      "nutrients" : {
        "ENERC_KCAL" : 89.0,
        "PROCNT" : 1.09,
        "FAT" : 0.33,
        "CHOCDF" : 22.84,
        "FIBTG" : 2.6
      },
      "category" : "Generic foods",
      "categoryLabel" : "food"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_unit",
      "label" : "Whole",
      "qualified" : [ [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_large",
        "label" : "large"
      } ], [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_medium",
        "label" : "medium"
      } ], [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_small",
        "label" : "small"
      } ], [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_extra_large",
        "label" : "extra large"
      } ] ]
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_serving",
      "label" : "Serving"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_cup",
      "label" : "Cup",
      "qualified" : [ [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_mashed",
        "label" : "mashed"
      } ], [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_sliced",
        "label" : "sliced"
      } ] ]
    } ]
  }, {
    "food" : {
      "foodId" : "food_aocnn91b13gt0mb3g65o1blp8uf1",
      "label" : "Banana Banana Bread",
      "nutrients" : {
        "ENERC_KCAL" : 248.9957855621517,
        "PROCNT" : 4.096864095708524,
        "FAT" : 9.584660352562652,
        "CHOCDF" : 37.89785426202022,
        "FIBTG" : 1.848914668962705
      },
      "category" : "Generic meals",
      "categoryLabel" : "meal",
      "foodContentsLabel" : "all-purpose flour; baking soda; salt; butter; brown sugar; eggs; bananas"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_unit",
      "label" : "Whole"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_serving",
      "label" : "Serving"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    } ]
  }, {
    "food" : {
      "foodId" : "food_apcxoi8abwi00ka8h3sbaa6woutg",
      "label" : "Ella's Kitchen Bananas, Bananas, Bananas, Super Smooth Banana Puree Bananas",
      "nutrients" : {
        "ENERC_KCAL" : 92.85714285714286,
        "PROCNT" : 1.4285714285714286,
        "CHOCDF" : 22.857142857142858,
        "FIBTG" : 2.857142857142857
      },
      "brand" : "Ella's Kitchen",
      "category" : "Packaged foods",
      "categoryLabel" : "food",
      "foodContentsLabel" : "ORGANIC BANANAS; <1% ORGANIC LEMON JUICE CONCENTRATE.",
      "image" : "https://www.edamam.com/food-img/36a/36a899bb7eb11fa5b00bfd7a66f70ea4.png"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_serving",
      "label" : "Serving"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_package",
      "label" : "Package"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    } ]
  }, {
    "food" : {
      "foodId" : "food_a3t5d1ubn0wxuxaddsz4oasllcdd",
      "label" : "Bananas, dehydrated, or banana powder",
      "nutrients" : {
        "ENERC_KCAL" : 346.0,
        "PROCNT" : 3.89,
        "FAT" : 1.81,
        "CHOCDF" : 88.28,
        "FIBTG" : 9.9
      },
      "category" : "Generic foods",
      "categoryLabel" : "food",
      "image" : "https://www.edamam.com/food-img/6bc/6bcf87ba7f4f162b0d257d041d69af34.jpg"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_tablespoon",
      "label" : "Tablespoon"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_cup",
      "label" : "Cup"
    } ]
  }, {
    "food" : {
      "foodId" : "food_btv060tam0bcs9ad0k678a1rg39d",
      "label" : "Banana Banana",
      "nutrients" : {
        "ENERC_KCAL" : 61.72943341176572,
        "PROCNT" : 0.5878993658263402,
        "CHOCDF" : 14.697484145658505,
        "FIBTG" : 0.5878993658263402
      },
      "brand" : "Maui Wowi Hawaiian Coffee & Smoothies",
      "category" : "Fast foods",
      "categoryLabel" : "meal"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_serving",
      "label" : "Serving"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    } ]
  }, {
    "food" : {
      "foodId" : "food_b0bn6w4ab49t55b1o8jsnbq6nm2g",
      "label" : "Bananas, raw",
      "nutrients" : {
        "ENERC_KCAL" : 89.0,
        "PROCNT" : 1.09,
        "FAT" : 0.33,
        "CHOCDF" : 22.84,
        "FIBTG" : 2.6
      },
      "category" : "Generic foods",
      "categoryLabel" : "food",
      "image" : "https://www.edamam.com/food-img/9f6/9f6181163a25c96022ee3fc66d9ebb11.jpg"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_unit",
      "label" : "Whole",
      "qualified" : [ [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_large",
        "label" : "large"
      } ], [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_medium",
        "label" : "medium"
      } ], [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_small",
        "label" : "small"
      } ], [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_extra_large",
        "label" : "extra large"
      } ] ]
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_cup",
      "label" : "Cup",
      "qualified" : [ [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_mashed",
        "label" : "mashed"
      } ], [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_sliced",
        "label" : "sliced"
      } ] ]
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_serving",
      "label" : "Serving"
    } ]
  }, {
    "food" : {
      "foodId" : "food_ate5r4tbsw6lhfarjwslfaeuoh95",
      "label" : "Banana",
      "nutrients" : {
        "ENERC_KCAL" : 89.0,
        "PROCNT" : 1.100000023841858,
        "FAT" : 0.30000001192092896,
        "CHOCDF" : 23.0,
        "FIBTG" : 2.5999999046325684
      },
      "brand" : "DOLE PACKAGED FOODS, LLC",
      "category" : "Packaged foods",
      "categoryLabel" : "food",
      "foodContentsLabel" : "Bananas",
      "image" : "https://www.edamam.com/food-img/2d8/2d8413480692de9716bff261a2fe0ad7.jpg"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_unit",
      "label" : "Whole"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    } ]
  }, {
    "food" : {
      "foodId" : "food_bwt1mopat6nxb0bxlt7g7azjnyyt",
      "label" : "Woodpecker Banana Chips Banana",
      "nutrients" : {
        "ENERC_KCAL" : 500.0,
        "PROCNT" : 3.3333333333333335,
        "FAT" : 26.666666666666668,
        "CHOCDF" : 66.66666666666667,
        "FIBTG" : 3.3333333333333335
      },
      "brand" : "Woodpecker",
      "category" : "Packaged foods",
      "categoryLabel" : "food",
      "foodContentsLabel" : "PREMIUM FRESH BANANAS; COCONUT OIL; WHITE REFINED SUGAR; BANANA FLAVORING",
      "image" : "https://www.edamam.com/food-img/03b/03b48d67059093b3a3e59594ae3e673c.png"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_serving",
      "label" : "Serving"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_piece",
      "label" : "Piece"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_package",
      "label" : "Package"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    } ]
  }, {
    "food" : {
      "foodId" : "food_bfmwoz8an2sv8bahepkdlbi54jco",
      "label" : "BANANA",
      "nutrients" : {
        "ENERC_KCAL" : 336.0,
        "FAT" : 1.0700000524520874,
        "CHOCDF" : 78.56999969482422,
        "FIBTG" : 1.7999999523162842
      },
      "brand" : "Kid Vids Educational Entertainment",
      "category" : "Packaged foods",
      "categoryLabel" : "food",
      "foodContentsLabel" : "BANANA; SULFUR DIOXIDE (AS A PRESERVATIVE). "
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    } ]
  }, {
    "food" : {
      "foodId" : "food_bajo89dbjzsqkdabpwfwzaykv520",
      "label" : "MARGIE'S, BANANA SYRUP, BANANA",
      "nutrients" : {
        "ENERC_KCAL" : 203.0,
        "PROCNT" : 1.350000023841858,
        "CHOCDF" : 47.29999923706055,
        "FIBTG" : 1.399999976158142
      },
      "brand" : "Margie's",
      "category" : "Packaged foods",
      "categoryLabel" : "food",
      "foodContentsLabel" : "WHITE GRAPE JUICE CONCENTRATE; BANANA PUREE; XANTHAN GUM (THICKENER).",
      "image" : "https://www.edamam.com/food-img/31a/31a9e703aafa5f40855d642fb15eaa82.jpg"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_cup",
      "label" : "Cup"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_milliliter",
      "label" : "Milliliter"
    } ]
  }, {
    "food" : {
      "foodId" : "food_at5js64bpj5aqlatwnxc1b3eky8g",
      "label" : "BANANA",
      "nutrients" : {
        "ENERC_KCAL" : 312.0,
        "PROCNT" : 12.5,
        "FAT" : 6.25,
        "CHOCDF" : 40.619998931884766,
        "FIBTG" : 6.199999809265137
      },
      "brand" : "Wonder Natural Foods Corp",
      "category" : "Packaged foods",
      "categoryLabel" : "food",
      "foodContentsLabel" : "PEANUTS (AS DEFATTED PEANUT FLOUR; PEANUT BUTTER AND NATURAL PEANUT OILS); PURE WATER; TAPIOCA SYRUP; RICE SYRUP; VEGETABLE GLYCERINE; CANE SUGAR; BANANA; NATURAL COLORS AND FLAVORS; SALT; CALCIUM CARBONATE; LECITHIN; TOCOPHEROL (VITAMIN E); SODIUM ASCORBATE (VITAMIN C).",
      "image" : "https://www.edamam.com/food-img/e54/e5400f8aa1026dfcad8bc71626640c0d.jpg"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_tablespoon",
      "label" : "Tablespoon"
    } ]
  }, {
    "food" : {
      "foodId" : "food_ahhuke5brcu82kbgsdnfnaz0ihz5",
      "label" : "BANANAS",
      "nutrients" : {
        "ENERC_KCAL" : 425.0,
        "PROCNT" : 5.0,
        "FAT" : 22.5,
        "CHOCDF" : 67.5,
        "FIBTG" : 10.0
      },
      "brand" : "Next Organics",
      "category" : "Packaged foods",
      "categoryLabel" : "food",
      "foodContentsLabel" : "ORGANIC DARK CHOCOLATE (ORGANIC CHOCOLATE LIQUOR; ORGANIC CANE SUGAR; ORGANIC COCOA BUTTER; SOY LECITHIN (NON-GMO); ORGANIC VANILLA EXTRACT); ORGANIC BANANA; ORGANIC CONFECTIONER'S GLAZE (ORGANIC MALTODEXTRIN; ORGANIC SUCROSE; SHELLAC).",
      "image" : "https://www.edamam.com/food-img/bce/bcee9aabe06600f7718c7385f9fa4e10.jpg"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_piece",
      "label" : "Piece"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    } ]
  }, {
    "food" : {
      "foodId" : "food_b0cn4dxbvicfniboeptaratve7kf",
      "label" : "Banana Colada",
      "nutrients" : {
        "ENERC_KCAL" : 110.88155256373531,
        "PROCNT" : 0.3145989687768548,
        "FAT" : 1.8132354626181608,
        "CHOCDF" : 12.634270982526496,
        "FIBTG" : 0.4520481237467774
      },
      "category" : "Generic meals",
      "categoryLabel" : "meal",
      "foodContentsLabel" : "bananas; cream of coconut; rum; banana liqueur; ice"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_unit",
      "label" : "Whole"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_serving",
      "label" : "Serving"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    } ]
  }, {
    "food" : {
      "foodId" : "food_a5uaedcau3zmr1bgt4opxaw98inz",
      "label" : "banana chip",
      "nutrients" : {
        "ENERC_KCAL" : 519.0,
        "PROCNT" : 2.3,
        "FAT" : 33.6,
        "CHOCDF" : 58.4,
        "FIBTG" : 7.7
      },
      "category" : "Generic foods",
      "categoryLabel" : "food"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_unit",
      "label" : "Whole"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_chip",
      "label" : "Chip"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_bag",
      "label" : "Bag"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_package",
      "label" : "Package"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_packet",
      "label" : "Packet"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    } ]
  }, {
    "food" : {
      "foodId" : "food_bpsku91ajk9ivaa2e2f65bq5u0cy",
      "label" : "banana flower",
      "nutrients" : {
        "ENERC_KCAL" : 376.3,
        "PROCNT" : 1.85,
        "FAT" : 0.5,
        "CHOCDF" : 91.1,
        "FIBTG" : 5.35
      },
      "category" : "Generic foods",
      "categoryLabel" : "food"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_unit",
      "label" : "Whole"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_blossom",
      "label" : "Blossom"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_flower",
      "label" : "Flower"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_cup",
      "label" : "Cup"
    } ]
  }, {
    "food" : {
      "foodId" : "food_a9mhph0akn8yk5ayay1a8b4jf8b5",
      "label" : "Banana Bites",
      "nutrients" : {
        "ENERC_KCAL" : 156.58739749676306,
        "PROCNT" : 2.3265990504963314,
        "FAT" : 3.183690116529996,
        "CHOCDF" : 31.020578334052654,
        "FIBTG" : 1.29831678895123
      },
      "category" : "Generic meals",
      "categoryLabel" : "meal",
      "foodContentsLabel" : "bananas; pancake"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_unit",
      "label" : "Whole"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_serving",
      "label" : "Serving"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    } ]
  }, {
    "food" : {
      "foodId" : "food_b7s1tmrbg8nh93bb4g2p8a6xjnq1",
      "label" : "Banana Foster",
      "nutrients" : {
        "ENERC_KCAL" : 223.71923225842139,
        "PROCNT" : 1.5217224404004648,
        "FAT" : 8.433818242159413,
        "CHOCDF" : 30.062203108579016,
        "FIBTG" : 1.27915260799823
      },
      "category" : "Generic meals",
      "categoryLabel" : "meal",
      "foodContentsLabel" : "Vanilla Ice Cream; Unsalted Butter; Light Brown Sugar; ground Cinnamon; Banana Liqueur; banana; Dark Rum"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_unit",
      "label" : "Whole"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_serving",
      "label" : "Serving"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    } ]
  }, {
    "food" : {
      "foodId" : "food_a4xw0lzaxjp0k6bdaotmcbpn9h60",
      "label" : "Banana Milk",
      "nutrients" : {
        "ENERC_KCAL" : 71.61894764466605,
        "PROCNT" : 2.406253772470804,
        "FAT" : 2.199036871801601,
        "CHOCDF" : 11.260261120587849,
        "FIBTG" : 0.9279622096837685
      },
      "category" : "Generic meals",
      "categoryLabel" : "meal",
      "foodContentsLabel" : "banana; vanilla; milk"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_unit",
      "label" : "Whole"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_serving",
      "label" : "Serving"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    } ]
  }, {
    "food" : {
      "foodId" : "food_adrvxqnbcnax8gbiobe5fafitoio",
      "label" : "banana liqueur",
      "nutrients" : {
        "ENERC_KCAL" : 308.0,
        "PROCNT" : 0.1,
        "FAT" : 0.3,
        "CHOCDF" : 32.2
      },
      "category" : "Generic foods",
      "categoryLabel" : "food"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_unit",
      "label" : "Whole"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_serving",
      "label" : "Serving"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_jigger",
      "label" : "Jigger"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_fluid_ounce",
      "label" : "Fluid ounce"
    } ]
  }, {
    "food" : {
      "foodId" : "food_azds4vwbq8u4boave9i3baksdmpd",
      "label" : "banana pepper",
      "nutrients" : {
        "ENERC_KCAL" : 27.0,
        "PROCNT" : 1.66,
        "FAT" : 0.45,
        "CHOCDF" : 5.35,
        "FIBTG" : 3.4
      },
      "category" : "Generic foods",
      "categoryLabel" : "food"
    },
    "measures" : [ {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_unit",
      "label" : "Whole",
      "qualified" : [ [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_medium",
        "label" : "medium"
      } ], [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_large",
        "label" : "large"
      } ], [ {
        "uri" : "http://www.edamam.com/ontologies/edamam.owl#Qualifier_small",
        "label" : "small"
      } ] ]
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pepper",
      "label" : "Pepper"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_gram",
      "label" : "Gram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_ounce",
      "label" : "Ounce"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_pound",
      "label" : "Pound"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_kilogram",
      "label" : "Kilogram"
    }, {
      "uri" : "http://www.edamam.com/ontologies/edamam.owl#Measure_cup",
      "label" : "Cup"
    } ]
  } ],
  "_links" : {
    "next" : {
      "title" : "Next page",
      "href" : "https://api.edamam.com/api/food-database/parser?session=40&ingr=banana&app_id=46308b95&app_key=cced96337a51df88ee66f2eb2b9c13c6"
    }
  }
}