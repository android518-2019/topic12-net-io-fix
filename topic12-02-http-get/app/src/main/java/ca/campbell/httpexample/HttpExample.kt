package ca.campbell.httpexample

import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.ByteArrayOutputStream
import java.io.DataOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.io.Reader
import java.io.UnsupportedEncodingException
import java.net.HttpURLConnection
import java.net.URL

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ScrollView
import android.widget.TextView

/*
 * HttpURLConnectionExample
 *
 * This app sends a URL uses HttpURLConnection class + GET
 * to download a web page and display some of it in a TextView
 *
 */
class HttpExample : Activity() {
    lateinit var urlText: EditText
    lateinit var textView: TextView
    lateinit var scrollv: ScrollView

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_http_example)

        urlText = findViewById<View>(R.id.myURL) as EditText
        textView = findViewById<View>(R.id.tv) as TextView
        scrollv = findViewById<View>(R.id.scrollv) as ScrollView
        // scrollv.visibility = View.GONE
    }
    // from https://antonioleiva.com/api-request-kotlin/
    fun chuckNorrisApi(view: View) {
        // HTTPURLConnection will not redirect http->https https->http
        // so check, swap or add http
        val stringUrl = "https://api.icndb.com/jokes/random"
        // http://api.icndb.com/jokes/random?firstName=Grace&lastName=Hopper
        Log.d(TAG, "url$stringUrl")
        // first check to see if we can get on the network

        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        if (networkInfo != null && networkInfo.isConnected) {
            // invoke the AsyncTask to do the dirtywork.
            DownloadWebpageText().execute(stringUrl)
            // text is set in DownloadWebpageText().onPostExecute()
        } else {
            textView.text = "No network connection available."
        }

        /*
        async {
            val result = URL(stringUrl).readText()
            uiThread {
                Log.d("Request", result)
                longToast("Request performed")
            }
        }
        */
    }
        /*
	 * When user clicks button, executes an AsyncTask to do the download. Before
	 * attempting to fetch the URL, makes sure that there is a network
	 * connection.
	 */
    fun myClickHandler(view: View) {
        // Gets the URL from the UI's text field.
        var stringUrl = urlText.text.toString()
        val HTTP = "http://"
        val HTTPS = "https://"
        val leng = HTTP.length - 1
        scrollv.visibility = View.VISIBLE
        if (stringUrl.isEmpty()) {
            textView.text = "Gimmey some URLz pleaze."

        } else {
            // HTTPURLConnection will not redirect http->https https->http
            // so check, swap or add http
            if (stringUrl.substring(0, HTTPS.length - 1) !== HTTPS)
                if (stringUrl.substring(0, leng) !== HTTP)
                    stringUrl = "https://$stringUrl"
                else
                // swap https for http
                    stringUrl = "https://" + stringUrl.substring(leng, stringUrl.length - 1)
            Log.d(TAG, "url$stringUrl")
            // first check to see if we can get on the network
            val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connMgr.activeNetworkInfo
            if (networkInfo != null && networkInfo.isConnected) {
                // invoke the AsyncTask to do the dirtywork.
                DownloadWebpageText().execute(stringUrl)
                // text is set in DownloadWebpageText().onPostExecute()
            } else {
                textView.text = "No network connection available."
            }
        }
    } // myClickHandler()

    /*
	 * Uses AsyncTask to create a task away from the main UI thread. This task
	 * takes a URL string and uses it to create an HttpUrlConnection. Once the
	 * connection has been established, the AsyncTask downloads the contents of
	 * the webpage via an an InputStream. The InputStream is converted into a
	 * string, which is displayed in the UI by the AsyncTask's onPostExecute
	 * method.
	 */

    private inner class DownloadWebpageText : AsyncTask<String, Void, String>() {

        // onPostExecute displays the results of the AsyncTask.
        // runs in calling thread (in UI thread)
        // we don't give the buffer to the UI thread until we have the whole thing
        // previous examples used a global variable for string buffer
        // so it was available as it filled.
        //  here the data is not available until it's done reading
        override fun onPostExecute(result: String) {
            textView.text = result

            Log.d(TAG, "onPostExecute() "+result)

        }

        override// runs in background (not in UI thread)
        fun doInBackground(vararg urls: String): String {
            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadUrl(urls[0])
            } catch (e: IOException) {
                Log.e(TAG, "exception thrown by download")
                return "Unable to retrieve web page. URL may be invalid."
            }

        }
    } // AsyncTask DownloadWebpageText()

    /*
	 * Given a URL, establishes an HttpUrlConnection and retrieves the web page
	 * content as a InputStream, which it returns as a string.
	 */

    @Throws(IOException::class)
    private fun downloadUrl(myurl: String): String {
        Log.d(TAG, "downloadUrl $myurl")

        var istream: InputStream? = null
        // Only read the first 500 characters of the retrieved
        // web page content.
        // int len = MAXBYTES;
        var conn: HttpURLConnection? = null
        val url = URL(myurl)
        try {
            // create and open the connection
            conn = url.openConnection() as HttpURLConnection

            /*
			 * set maximum time to wait for stream read read fails with
			 * SocketTimeoutException if elapses before connection established
			 *
			 * in milliseconds
			 *
			 * default: 0 - timeout disabled
			 */
            conn.readTimeout = 10000
            /*
			 * set maximum time to wait while connecting connect fails with
			 * SocketTimeoutException if elapses before connection established
			 *
			 * in milliseconds
			 *
			 * default: 0 - forces blocking connect timeout still occurs but
			 * VERY LONG wait ~several minutes
			 */
            conn.connectTimeout = 15000
            /*
			 * HTTP Request method defined by protocol
			 * GET/POST/HEAD/POST/PUT/DELETE/TRACE/CONNECT
			 *
			 * default: GET
			 */
            conn.requestMethod = "GET"
            // specifies whether this connection allows receiving data
            conn.doInput = true
            // Starts the query
            conn.connect()

            val response = conn.responseCode
            Log.d(TAG, "Server returned: $response")

            /*
			 *  check the status code HTTP_OK = 200 anything else we didn't get what
			 *  we want in the data.
			 */
            if (response != HttpURLConnection.HTTP_OK)
                return "Server returned: $response aborting read."

            // get the stream for the data from the website
            istream = conn.inputStream
            // read the stream, returns String
            return readIt(istream)
        } catch (e: IOException) {
            Log.e(TAG, "IO exception in bg")
            Log.getStackTraceString(e)
            throw e
        } finally {
            /*
			 * Make sure that the InputStream is closed after the app is
			 * finished using it.
			 * Make sure the connection is closed after the app is finished using it.
			 */

            if (istream != null) {
                try {
                    istream.close()
                } catch (ignore: IOException) {
                }

                if (conn != null)
                    try {
                        conn.disconnect()
                    } catch (ignore: IllegalStateException) {
                    }

            }
        }
    } // downloadUrl()

    //
    /*
	 * Reads stream from HTTP connection and converts it to a String.
	 *  We use a BufferedInputStream to read the data from the http connection InputStream
	 *  into our buffer  (NETIOBUFFER bytes at a time, I often choose 1KiB for
	 *  probably small datastreams.)
	 *
	 *  We then use a ByteArrayOutputStream + DataOutputStream.writer to collect the data
	 *  and write it to a byte array.
	 *  The reason this is done is that we do not know the length of the incoming data
	 *  So the OutputStream components allow us to put data into and grow the buffer.
	 *
	 *  It is unfortunate that in Java everything is a pointer but we do not have access
	 *  to the underlying structures.  If this is done in c or c++ we allocate, manage,
	 *  grow and release the buffers as we need them.
	 *
	 *  In java using these classes has that effect.  It is not necessarily efficient,
	 *  a better solution may be needed for large data exchange.
	 */
    @Throws(IOException::class, UnsupportedEncodingException::class)
    fun readIt(stream: InputStream?): String {
        var bytesRead: Int
        var totalRead = 0
        val buffer = ByteArray(NETIOBUFFER)

        // for data from the server
        val bufferedInStream = BufferedInputStream(stream!!)
        // to collect data in our output stream
        val byteArrayOutputStream = ByteArrayOutputStream()
        val writer = DataOutputStream(byteArrayOutputStream)

        // read the stream until end
        /*
        bytesRead = 0
        while (bytesRead != 1) {
            bytesRead = bufferedInStream.read(buffer)
            Log.d(TAG, "reading "+ buffer)
            if(bytesRead >0) {
                writer.write(buffer)
                totalRead += bytesRead
            }
        }

         */
        bytesRead = bufferedInStream.read(buffer)
        writer.write(buffer)
        totalRead += bytesRead
        writer.flush()
        Log.d(TAG, "Bytes read: " + totalRead
                + "(-1 means end of reader so max of)")

        return byteArrayOutputStream.toString()
    } // readIt()

    companion object {
        private val NETIOBUFFER = 1024

        private val TAG = "HttpURLConn"
    }

} // Activity class