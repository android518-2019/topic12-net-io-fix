# topic12 network i/o repos

## topic12-net-checkistatus

most components deprecated

Sample code using ConnectivityManager and NetworkInfo

Note:  I cannot update to using the components that 
are deprecated for API 29 because the libs require
a min api of 23, due to the Samsungs being stuck at 22\
If I change most student tablets will not work
### app NetworkCheckStatus

Use of logcat to watch the logging
```
adb logcat -s NETCHK
```
## topic12-net i/o http GET
working
Sample code that opens an HTTP connection (socket) to a 
web page, then retrieves the content which
is displayed as text in the UI, normally this would be
formatted and used properly.  Try an api or any website.
see Note for net-checkstatus

Uses 
* ConnectivityManager and NetworkInfo
* AsyncTask 
* HttpURLConnection for get
* BufferedInputStream
* ByteArrayOutputStream
* DataOutputStream
### app HttpUrlConnection
Note: input URL cannot be https:

Use of logcat to watch the logging
```
adb logcat -s HttpURLConn
```
## topic12-net net i/o HTTPS POST
working
Sample code that opens an HTTP connection (socket) to a 
a website api over TLS so using HTTPS 
It needs 2  api key, an api appid POST data is constructed
for the request, uses <http://www.edamam.com> api 

n.b. HTTP POST is more secure as you do not expose data in the URL
as you do with HTTP GET
The app retrieves the JSON response  which
is displayed as text in the UI, normally this would be
parsed and used otherwise in the UI.

Uses 
* ConnectivityManager and NetworkInfo
* AsyncTask 
* URLEncoder
* HttpURLConnection for POST
* BufferedInputStream
* ByteArrayOutputStream
* DataOutputStream

### app HttpUrlConnection
Use of logcat to watch the logging
converted to kotlin there are problems the api has changed
so I need to fix the code to use the changes
see the scripts in assets for more information
```
adb logcat -s HttpURLPOST
```
