package ca.campbell.networkcheckstatus

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.NetworkInfo.State
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView

/*
 * This app is intended to be an example of checking network connectivity.
 * It does not do any network access, though this is the normal next step.
 *
 * When using networking it is important to verify that you have an active
 * connected network connection before doing any network activity.
 *
 * Since it checks connectivity but does no network access it does not need
 * to be done in an AsyncTask.  Any network access (data over the network)
 *  must be done in a background thread.
 *
 * Ref
 * https://developer.android.com/training/monitoring-device-state/connectivity-monitoring.html
 *
 * Permissions required in AndroidMainifest.xml:
 * <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE">
 * </uses-permission>
 */
class MainActivity : Activity() {
    lateinit var tv2: TextView
    lateinit var tv3: TextView
    private var networkIsUp = false
    internal var networkInfo: NetworkInfo? = null
    internal var connMgr: ConnectivityManager? = null

    /*
	*   More types found here
	*   https://developer.android.com/reference/android/net/ConnectivityManager.html
	*/
    private// get an instance of ConnectivityManager
    // check if wifi is available
    // allows connection to WLAN
    // uses radio frequency bands
    // IEEE 802.11(a and a variants, b, g, n)
    // check if mobile is available
    // mobile telephony
    // mobile phones connect to a cellular network of base stations which are
    // then connected to the PSTN  (public switched telephone network)
    // Comm between cell site and phone include GSM CDMA AND TDMA
    // check if bluetooth is available
    // named after Harold Bluetooth Danish king 10th century
    // IEEE 802.15.1
    // short distance wireless comm, developed by Ericcson (~1994)
    // check if WiMAX is available
    // Worldwide Interoperability for Microwave Accesss
    // IEEE 802.16
    // provides multiple physical and media access control options
    // check if Ethernet is available
    // Physically wired network using Cat5/6 cables with RJ45 connectors
    // unlikely on a mobile device
    // IEEE 802.3
    // Ethernet developed at Xerox PARC (~1973)
    val availablilty: String
        get() {
            val wifiConn: Boolean
            val mobileConn: Boolean
            var genNetConn: Boolean
            var netInfo: NetworkInfo?
            var msg: String
            connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            netInfo = connMgr!!.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
            if (netInfo != null) {
                wifiConn = netInfo.isAvailable
            } else {
                wifiConn = false
            }
            Log.d(TAG, "Wifi available: $wifiConn")
            msg = "Wifi available: $wifiConn"
            netInfo = connMgr!!.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
            if (netInfo != null) {
                mobileConn = netInfo.isAvailable
            } else {
                mobileConn = false
            }
            msg += "\n Mobile available: $mobileConn"
            Log.d(TAG, "Mobile available: $mobileConn")
            netInfo = connMgr!!.getNetworkInfo(ConnectivityManager.TYPE_BLUETOOTH)
            if (netInfo != null) {
                genNetConn = netInfo.isAvailable
            } else {
                genNetConn = false
            }
            msg += "\n BlueTooth available: $genNetConn"
            Log.d(TAG, "BlueTooth available: $genNetConn")
            netInfo = connMgr!!.getNetworkInfo(ConnectivityManager.TYPE_WIMAX)
            if (netInfo != null) {
                genNetConn = netInfo.isAvailable
            } else {
                genNetConn = false
            }
            msg += "\n WiMax available: $genNetConn"
            Log.d(TAG, "WiMax available: $genNetConn")
            netInfo = connMgr!!.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET)
            if (netInfo != null) {
                genNetConn = netInfo.isAvailable
            } else {
                genNetConn = false
            }
            msg += "\n Ethernet available: $genNetConn"
            Log.d(TAG, "Ethernet available: $genNetConn")
            return msg
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tv2 = findViewById(R.id.tv2) as TextView
        tv3 = findViewById(R.id.tv3) as TextView
    }

    fun checkStatusButton(view: View) {
        checkStatus()
    }

    /*
	 * checkStatus() using an instance of ConnectivityManager via the
	 * Connectivity Service which is an Android Service. Checks if network is
	 * live, sets networkIsUp depending on network state.
	 *
	 * Connectivity Manager Class
	 *
	 * gives access to the state of network connectivity. Can be used to notify
	 * apps when connectivity changes
	 *
	 * -monitor network connections (wi-fi, GPRS, UMTs, etc
	 *
	 * -send broadcast intents when connectivity changes
	 *
	 * -trys to failover to another net when connectivity lost
	 *
	 * -api for network state connectivity
	 *
	 * ConnectivityManager.getActiveNetworkInfo()
	 *
	 * returns details about the current active default network (NetworkInfo)
	 * null if no default network ALWAYS check isConnected() before initiating
	 * network traffic
	 *
	 * requires perm: android.permission.ACCESS_NETWORK_STATE
	 *
	 * NetworkInfo class
	 *
	 * gives access information about the status of a network interface
	 * connection
	 */
    fun checkStatus() {
        connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        // getActiveNetworkInfo() each time as the network may swap as the
        // device moves
        if (connMgr != null) {
            networkInfo = connMgr!!.activeNetworkInfo
            // ALWAYS check isConnected() before initiating network traffic
            if (networkInfo != null && networkInfo!!.isConnectedOrConnecting) {
                tv2.text = "Network is connected or connecting"
                networkIsUp = true
            } else {
                tv2.text = "No network connectivity"
                networkIsUp = false
            }
        } else {
            tv2.text = "No network manager service"
            networkIsUp = false
        }
    } // checkStatus()

    fun checkEverything(view: View) {
        var message: String? = null
        var state: State? = null
        checkStatus()
        if (networkIsUp) {
            // deprecated in API 28  (released 2018-08-06)
            // TODO in future use instead ConnectivityManager.NetworkCallback API
            state = networkInfo!!.state
        }
        if (state == null) {
            tv2.text = "Unable to get state info"
        } else {
            tv2.text
            when (networkInfo!!.state) {
                NetworkInfo.State.CONNECTED -> message = "Connected"
                NetworkInfo.State.CONNECTING -> message = "Connecting"
                NetworkInfo.State.DISCONNECTED -> message = "Disconnected"
                NetworkInfo.State.DISCONNECTING -> message = "Disconnecting"
                NetworkInfo.State.SUSPENDED -> message = "Suspened"
                NetworkInfo.State.UNKNOWN -> message = "Unknown"
                else -> message = "No valid State found"
            }

            tv2.text = "State: $message"
        }

    } // checkEverything()

    fun checkEverythingDetailed(view: View) {
        var message: String? = null
        var state: State? = null
        checkStatus()
        if (networkIsUp) {
            state = networkInfo!!.state
        }
        if (state == null) {
            tv2.text = "Unable to get state info"
        } else {
            when (networkInfo!!.detailedState) {
                NetworkInfo.DetailedState.IDLE -> message = "Idle"
                NetworkInfo.DetailedState.CONNECTED -> message = "Connected"
                NetworkInfo.DetailedState.CONNECTING -> message = "Connecting"
                NetworkInfo.DetailedState.SCANNING -> message = "Scanning: searching for an access point"
                NetworkInfo.DetailedState.DISCONNECTED -> message = "Disconnected"
                NetworkInfo.DetailedState.DISCONNECTING -> message = "Connected"
                NetworkInfo.DetailedState.AUTHENTICATING -> message = "Authenticating"
                NetworkInfo.DetailedState.BLOCKED -> message = "Block"
                NetworkInfo.DetailedState.OBTAINING_IPADDR -> message = "awaiting DHCP response"
                NetworkInfo.DetailedState.FAILED -> message = "Failed"
                NetworkInfo.DetailedState.VERIFYING_POOR_LINK -> message = "Link has poor connectivity"
                else -> message = "No valid State found"
            }
            // Ex types WIFI, Bluetooth, Mobile...
            val typeName = networkInfo!!.typeName
            message = "$message\n Network Type $typeName"
            if (networkInfo!!.type == ConnectivityManager.TYPE_MOBILE) {
                // subtype applies only to type mobile
                // see TelephonyManager class for enum of types
                val subtypeName = networkInfo!!.subtypeName
                message = "$message\n Network Subtype: $subtypeName"
            }
            val extraInfo = networkInfo!!.extraInfo
            message = "$message\n Extra Info $extraInfo"
            tv2.text = "Detailed State: \n $message"
        }
        tv3.text = "Available \n $availablilty"
    } // checkEverythingDetailed()

    companion object {
        private val TAG = "NETCHK"
    }

} // MainActivity
